#ifndef LINKEDLISTH
#define LINKEDLISTH
#define CV_IGNORE_DEBUG_BUILD_GUARD

#define ZERO 0
#define FALSE 0
#define TRUE !FALSE
#define MAX_LEN 50
#define END_STR 1
#define ONE 1
#define EQUAL 0

#include <stdio.h>

// Frame struct
typedef struct Frame
{
	char*		name;
	unsigned int	duration;
	char*		path;  
} Frame;


// Link (node) struct
typedef struct FrameNode
{
	Frame* frame;
	struct FrameNode* next;
} FrameNode;

/*
goal: to add new frame to the video
input: pointer to the video list
output: none
*/
void addFrame(FrameNode** head);


/*
goal: to change path for specific frame
input: pointer to the video list, name to change it path, path to change to
output: none
i am sending to function only after checking the name is in the list
*/
void changeFramePath(FrameNode** list, char* name, char* newPath);

/*
goal: to change name for specific frame
input: pointer to the video list, name to change it name, name to change to
output: none
i am sending to function only after checking the name is in the list
*/
void changeFrameName(FrameNode** list, char* name, char* newName);

/*
goal: to insert new frame at list in specific index
input: the list, the frame to enter, the index to enter the frame at
output: none
*/
void insertToIndex(FrameNode** head, FrameNode* newNode, int index);

/*
goal: to add a frame to the index that entered by name
this function saves the frame and than deletes it by it name and sends it to the function that insert new one
input: head - the head of the list to insert the frame, name - the name to delete and then return after, index - where to insert the new one
output: none
i am sending to function only after checking the name is in the list
*/
void insertToLocation(FrameNode** head, char* name, int index);

/*
goal: to reverse the list (first last, last first etc)
input: the head of the line
output: none
*/
void endToStart(FrameNode** head);

/*
goal: to create a Frame by asking the user for it and to insert it to the list of the movie
input: name and age of the frame pointer to list to insert the newFrame to 
output: 
*/
FrameNode* createFrame(char* name, int duration, char* path);

/*
goal: to add a frame to the numbers list
input: head - pointer to the first node of the list, newNode - the new frame to add
output: none
*/
void insertAtEnd(FrameNode** head, FrameNode* newNode);

/*
goal: to delete the frame
input: pointer to the video list, the name to search and delete
output: none
i am sending to function only after checking the name is in the list
*/
void deleteFrame(FrameNode** list, char* name);

/*
goal: to change duration for specific frame
input: pointer to the video list, name to change it duration, duration to change to
output: none
i am sending to function only after checking the name is in the list
*/
void changeFrameDuration(FrameNode** list, char* name, int duration);

/*
goal: to remove the first frame
input: the head of the list
output: none
*/
void delteFirstFrame(FrameNode** head);

/*
goal: to check if file is exist(it is bunary because we use here only binary files
input: the file to check
output: if the file exist\ openable (0 for no else for yes)
*/
int binaryFileExist(char* filename);

/*
goal: to change all the duration values in list
input: list, duration to change to
output: none
*/
void changeAllDuration(FrameNode** list, int duration);
/*
Function will perform the fgets command and also remove the newline
that might be at the end of the string - a known issue with fgets.
input: the buffer to read into, the number of chars to read
*/
void myFgets(char str[], int n);
#endif
