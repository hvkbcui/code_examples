#ifndef FILESAVEH
#define FILESAVEH
#include "linkedList.h"

/*
goal: to print the menu thats ask if to create new project or existing project
input: none
output: users choice(0 or 1)
*/
int printSaveMenu();

/*
goal: to print the choices in creation menu
input: none
output: none
*/
void printSaveMenuChoices();

/*
goal: open existing project
input: path to open
output: 0 if could not open file, else if it open
*/
int openExist(char* path);

/*
goal: to save the project what actually happens is i am writing in path that entered the details of the list seperated by comma and \n
input: nodes of frame, path to save the project at
output: none
i check and do the save in same function because i do not want to open path twice so i am doing all after opening the file
*/
void saveProject(FrameNode* list, char* file_path);

/*
goal: return the FrameNode with the details
input: file path
output: the frameNode with the details
*/
FrameNode* retFrame(char* path);

/*
goal: to check if file is exist
input: the file to check
output: if the file exist\ openable (0 for no else for yes)
*/
int textFileExist(char* path);

#endif
#pragma once
