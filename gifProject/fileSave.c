#define CV_IGNORE_DEBUG_BUILD_GUARD


#include <stdio.h>
#include <stdlib.h>
#include "fileSave.h"
#include "unchange.h"

#define NEW_PRO 0
#define EXIST_PRO 1
#define FIRST 1
#define ONE_CHAR 1
#define BASE_TEN 10
#define CHAR_BEFORE -1

/*
goal: to print the menu thats ask if to create new project or existing project
input: none
output: users choice(0 or 1)
*/
int printSaveMenu() {
	int choice = 0;
	printSaveMenuChoices();
	scanf("%d", &choice);
	getchar();
	while (choice != NEW_PRO && choice != EXIST_PRO) {
		printf("Invalid choice, try again:\n");
		printSaveMenuChoices();
		scanf("%d", &choice);
		getchar();
	}
	return choice;
}

/*
goal: to print the choices in creation manu
input: none
output: none
*/
void printSaveMenuChoices() {
	printf(" [0] Create a new project\n");
	printf(" [1] Load existing project\n");
}

/*
goal: open existing project
input: path to open 
output: 0 if could not open file, else if it open
*/
int openExist(char* path) {
	int res = 0;
	printf("Enter the path of the project (including project name):\n");
	myFgets(path, MAX_LEN);
	res = textFileExist(path);
	return res;
}

/*
goal: to save the project what actually happens is i am writing in path that entered the details of the list seperated by comma and \n
input: nodes of frame, path to save the project at
output: none
i check and do the save in same function because i do not want to open path twice so i am doing all after opening the file
*/
void saveProject(FrameNode* list, char* file_path) {
	FILE* file = NULL;
	char* name = NULL;
	char* path = NULL;
	char duration[MAX_LEN] = "";
	char ch = ' ';
	int i = 0, len = 0, durationInt = 0;
	name = (char*)malloc(sizeof(char) * MAX_LEN + ONE_CHAR);
	path = (char*)malloc(sizeof(char) * MAX_LEN + ONE_CHAR);
	file = fopen(file_path, "w");
	if (file == NULL) {
		printf("Error! canot create file");
		return;
	}
	while (list != NULL) {
		strcpy(path, list->frame->path);
		len = strlen(path);
		for (i = ZERO; i < len; i++) {// while path is not empty 
			fputc(path[i], file);//put each char in file
		}
		fputc(',', file);// seperate between path and duration
		durationInt = list->frame->duration;
		_itoa(durationInt, duration, BASE_TEN);//convert from int to string
		len = strlen(duration);
		for (i = ZERO; i < len; i++) {
			fputc(duration[i], file);
		}
		fputc(',', file);// seperate between duration and name
		strcpy(name, list->frame->name);
		len = strlen(name);
		for (i = ZERO; i < len; i++) {
			fputc(name[i], file);
		}
		fputc('\n', file);// seperate between one frame to another
		list = list->next;
	}
	fclose(file);

}

/*
goal: return the FrameNode with the details from the file 
input: file path
output: the frameNode with the details
sending this function after checking the path is openable
*/
FrameNode* retFrame(char* path) {
	FILE* file = NULL;
	FrameNode* first = NULL;
	FrameNode* newNode = NULL;
	int durationInt = 0, check = 0;
	char ch = ' ';
	char framePath[MAX_LEN] = "";
	char duration[MAX_LEN] = "";
	char name[MAX_LEN] = "";
	first = (FrameNode*)malloc(sizeof(FrameNode));
	newNode = (FrameNode*)malloc(sizeof(FrameNode));
	file = fopen(path, "r");
	check = FIRST;
	if (file == NULL) {
		return NULL;
	}
	else {
		while (check != FALSE) {// not end of file
			strcpy(framePath, "");// reset all the variables
			strcpy(name, "");
			strcpy(duration, "");
			while ((ch = (char)fgetc(file)) != ',' && ch != EOF)
			{
				strncat(framePath, &ch, ONE_CHAR);
			}
			while ((ch = (char)fgetc(file)) != ',' && ch != EOF)
			{
				strncat(duration, &ch, ONE_CHAR);
			}
			while ((ch = (char)fgetc(file)) != '\n' && ch != EOF)
			{
				strncat(name, &ch, 1);
			}
			durationInt = atoi(duration);// convert from string to int
			if (check == FIRST) {//first of the list wich means we create one and not inserting at end
				first = createFrame(name, durationInt, framePath);
				check++;// making it not first
			}
			else {
				newNode = createFrame(name, durationInt, framePath);
				insertAtEnd(&first, newNode);
			}
			if (ch = (char)fgetc(file) == EOF) {// if next char is end of file make check false
				check = FALSE;
			}
			else {//else go back one spot so we do not miss nothing
				fseek(file, CHAR_BEFORE, SEEK_CUR);
			}
		}
	}
	fclose(file);
	return first;
}


/*
goal: to check if file is exist
input: the file to check
output: if the file exist\ openable (0 for no else for yes)
*/
int textFileExist(char* path) {
	FILE* file = NULL;
	if (file = fopen(path, "r")) {
		fclose(file);
		return TRUE;
	}
	return FALSE;
}
