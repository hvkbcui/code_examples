#define CV_IGNORE_DEBUG_BUILD_GUARD

#include "unChange.h"
#pragma warning( disable : 4996 )

#define ONE 1
#define FALSE 0
#define TRUE !FALSE
#define EXIT 0
#define EQUAL 0
#define NUM_REPITATION_NUMBER 12

/*
goal: to print menu and return choice
input: none
output: choice
*/
int menuChoice() {
	int choice = 0;
	printMenu();
	scanf("%d", &choice);
	getchar();
	while (choice < EXIT || choice > NUM_REPITATION_NUMBER) {
		printf("You should type one of the options - 0-12!\n");
		printMenu();
		scanf("%d", &choice);
		getchar();
	}
	return choice;
}

/*
goal: to print menu 
input: none
output: none
*/
void printMenu() {

	printf("\nWhat would you like to do?\n");
	printf("[0] Exit\n");
	printf("[1] Add new Frame\n");
	printf("[2] Remove a Frame\n");
	printf("[3] Change frame index\n");
	printf("[4] Change frame duration\n");
	printf("[5] Change duration of all frames\n");
	printf("[6] List frames\n");
	printf("[7] Play movie!\n");
	printf("[8] Save project\n");
	printf("[9] reverse movie(bonus)\n");
	printf("[10] change frame name(bonus)\n");
	printf("[11] change frame path(bonus)\n");
	printf("[12] change gif repeatation(bonus)\n");
}

/*
goal: to search for frame with his frame name in list of frames
input: linked list with frames, name to find
output: 0 if name was not found else if found
*/
int searchName(FrameNode* list, char* name) {
	while (list) {
		if (strcmp(list->frame->name, name) == EQUAL) {
			return TRUE;
		}
		list = list->next;
	}
	return FALSE;
}

/*
goal: to print the list frames
input: nodes of frame
output: none
*/
void printList(FrameNode* list)
{
	printf("                Name            Duration        Path\n");
	while (list != NULL)
	{
		printf("\t\t%s\t\t%u ms\t\t%s\n", list->frame->name, list->frame->duration, list->frame->path);
		list = list->next;
	}
	printf("\n\n");
}

/*
goal: to return how many frames there is in the list
input: head - the first node of the list
output: how many frames
*/
int getLineLength(FrameNode* head)
{
	int res = 0;
	while (head != NULL) {
		res += ONE;
		head = head->next;
	}
	return res;
}


/*
Function will perform the fgets command and also remove the newline
that might be at the end of the string - a known issue with fgets.
input: the buffer to read into, the number of chars to read
*/
void myFgets(char str[], int n)
{
	fgets(str, n, stdin);
	str[strcspn(str, "\n")] = ZERO;
}

/*
goal: to get the name in the delete action and return if the frame is in the list
input: the list to search name, the name pointer to enter the name and to check if is in the list 
output: 0 if the name is not in the list else if in the list
*/
int getNameDelete(FrameNode* head, char* name) {
	printf("Enter the name of the frame you wish to erase\n");
	myFgets(name, MAX_LEN);
	if (!searchName(head, name)) {
		printf("The frame was not found\n");
		return FALSE;
	}
	return TRUE;
}
/*
goal: to get the name and index  in the index action and return if the frame is in the list
input: the list to search name, the name pointer to enter the name and to check if is in the list, and the index pointer to get from the user so we can insert the value of the index directely to main
output: 0 if the name is not in the list else if in the list
*/
int getNameIndex(FrameNode* head, char* name, int* index) {
	int len = 0;
	printf("Enter the name of the frame\n");
	myFgets(name, MAX_LEN);
	if (!searchName(head, name)) {
		printf("this frame does not exist\n");
		return FALSE;
	}
	printf("Enter the new index in the movie you wish to place the frame\n");
	scanf("%d", index);
	getchar();
	if ((*index) < ONE) {
		printf("Index can not be smaller than 1!\n");
		return FALSE;
	}
	len = getLineLength(head);
	if(len < (*index)) {
		printf("The movie contains only %d frames!\n", len);
		return FALSE;
	}
	return TRUE;
}

/*
goal: to change duration for specific frame
input: pointer to the video list, name to change it duration, duration to change to
output: none
*/
int getFrameDuration(FrameNode** head, char* name, int* duration) {
	printf("enter the name of the frame\n");
	myFgets(name, MAX_LEN);
	if (!searchName(head, name)) {
		printf("The frame does not exist\n");
		return FALSE;
	}
	printf("Enter the new duration\n");
	scanf("%d", duration);
	getchar();
	if ((*duration) < FALSE) {
		printf("Please pick positive time!\n");
		return FALSE;
	}
	return TRUE;
}

/*
goal: to change name for specific frame name
input: pointer to the video list, name to change it name, new name to change to 
output: if the values are ok or not ( 0 if not else if yes)
*/
int getFrameName(FrameNode** head, char* name, char* newName) {
	printf("enter the name of the frame\n");
	myFgets(name, MAX_LEN);
	if (!searchName(head, name)) {
		printf("The frame does not exist\n");
		return FALSE;
	}
	printf("Enter the new name:\n");
	myFgets(newName, MAX_LEN);
	if (searchName(head, newName)) {
		printf("The name already exists\n");
		return FALSE;
	}
	return TRUE;
}

/*
goal: to change path for specific frame
input: pointer to the video list, name to change it path, new path to change to 
output: if the values are ok or not ( 0 if not else if yes)
*/
int getFramePath(FrameNode** head, char* name, char* newPath) {
	printf("enter the name of the frame\n");
	myFgets(name, MAX_LEN);
	if (!searchName(head, name)) {
		printf("The frame does not exist\n");
		return FALSE;
	}
	printf("Enter the new path:\n");
	myFgets(newPath, MAX_LEN);
	if (!binaryFileExist(newPath)) {
		printf("Can't find file! Frame will not be changed\n");
		return FALSE;
	}
	return TRUE;
}

/*
goal: to change duration for all frames
input: pointer to the video list, duration to change all frames to
output: none
*/
int getDurationForAll(FrameNode** head, int* duration) {
	printf("Enter duration:");
	scanf("%d", duration);
	getchar();
	if ((*duration) < FALSE) {
		printf("Please pick positive time!\n");
		return FALSE;
	}
	return TRUE;
}
