#define CV_IGNORE_DEBUG_BUILD_GUARD


#include "linkedList.h"
#include "unchange.h"
#pragma warning( disable : 4996 )

/*
goal: to add new frame to the video
input: pointer to the video head
output: none
*/
void addFrame(FrameNode** head) {
	FrameNode* newFrame = NULL;
	char* path  = (char*)malloc(sizeof(char) * MAX_LEN + END_STR);
	char* name  = (char*)malloc(sizeof(char) * MAX_LEN + END_STR);
	int duration = 0;
	newFrame = (FrameNode*)malloc(sizeof(FrameNode));
	newFrame->frame = (Frame*)malloc(sizeof(Frame));
	newFrame->frame->name = (char*)malloc(sizeof(char) * MAX_LEN + END_STR);
	newFrame->frame->path = (char*)malloc(sizeof(char) * MAX_LEN + END_STR);
	printf("*** Creating new frame ***\n");
	printf("Please insert frame path:\n");
	myFgets(path, MAX_LEN);
	printf("Please insert frame duration(in miliseconds):\n");
	scanf("%d", &duration);
	getchar();
	while(duration < ZERO) {
		printf("Please pick positive time!\n");
		scanf("%d", &duration);
		getchar();
	}
	printf("Please choose a name for that frame:\n");
	myFgets(name, MAX_LEN);
	if (binaryFileExist(path) == FALSE) {
		printf("Can't find file! Frame will not be added\n");
		return;
	}
	while(searchName(*head, name) != FALSE) {
		printf("The name is already taken, please enter another name\n");
		myFgets(name, MAX_LEN);
	}
	newFrame = createFrame(name, duration, path);
	free(path);
	free(name);
	insertAtEnd(head, newFrame);
}

/*
goal: to create a Frame by asking the user for it and to insert it to the list of the movie
input: name and age of the frame pointer to list to insert the newFrame to
output:
*/
FrameNode* createFrame(char* name, int duration, char* path)
{
	FrameNode* newFrame = (FrameNode*)malloc(sizeof(FrameNode));
	newFrame->frame = (Frame*)malloc(sizeof(Frame));
	newFrame->frame = (Frame*)malloc(sizeof(Frame));
	newFrame->frame->name = (char*)malloc(sizeof(char) * (MAX_LEN + END_STR));
	newFrame->frame->path = (char*)malloc(sizeof(char) * (MAX_LEN + END_STR));
	strncpy(newFrame->frame->name, name, MAX_LEN);
	newFrame->frame->duration = duration;
	strncpy(newFrame->frame->path, path, MAX_LEN);
	newFrame->next = NULL;
	return newFrame;
}

/*
goal: to reverse the list (first last, last first etc)
input: the head of the line
output: none
*/
void endToStart(FrameNode** head) {
	FrameNode* last = NULL;
	FrameNode* cur = NULL;
	FrameNode* next = NULL;
	cur = (*head);
	while (cur != NULL) {
		next = cur->next;
		cur->next = last;// making cur be alone
		last = cur;//noe last is the alone cur
		cur = next;
	}
	*head = last;// and now everything is reversed
}

/*
goal: to change all the duration values in list
input: list, duration to change to
output: none
*/
void changeAllDuration(FrameNode** list, int duration) {
	FrameNode* first = NULL;
	first = (*list);
	while (first) {
		first->frame->duration = duration;
		first = first->next;
	}
}

/*
goal: to add a frame to the numbers list
input: head - pointer to the first node of the list, newNode - the new frame to add
output: none
*/
void insertAtEnd(FrameNode** head, FrameNode* newNode)
{
	if (*head == NULL)
	{
		*head = newNode;
		newNode->next = NULL;
	}
	else
	{
		FrameNode* p = *head;// so we wont make head shorter we use pointer
		while (p->next)
		{
			p = p->next;
		}
		p->next = newNode;// when we reached end
	}
}

/*
goal: to delete the frame
input: pointer to the video list, the name to search and delete 
output: none
i am sending to function only after checking the name is in the list 
*/
void deleteFrame(FrameNode** list, char* name) {
	FrameNode* first = NULL;
	FrameNode* nextOne = NULL;
	first = (*list);
	if ((strcmp(first->frame->name, name) == EQUAL)) {// if the name is in the first frame
		delteFirstFrame(list);
	}
	else {
		while (first->next != NULL) {
			nextOne = first->next;//so we can compare it without moving it
			if (strcmp((nextOne->frame->name), name) == EQUAL) {
				first->next = nextOne->next;
				free(nextOne);//remove the memory of the one to delete
				nextOne = NULL;
				return;
			}
			first = first->next;
		}
	}
}

/*
goal: to add a frame to the index that entered by name
this function saves the frame and than deletes it by it name and sends it to the function that insert new one
input: head - the head of the list to insert the frame, name - the name to delete and then return after, index - where to insert the new one
output: none
i am sending to function only after checking the name is in the list 
*/
void insertToLocation(FrameNode** head, char* name, int index)
{
	int i = 0;
	FrameNode* first = (*head);// pointer so we want delete when we move the list to next one
	FrameNode* move = (FrameNode*)malloc(sizeof(FrameNode));// the node to move to the new index
	while (first) {
		if (strcmp((first->frame->name), name) == EQUAL) {
			move = createFrame(first->frame->name, first->frame->duration, first->frame->path);//saving the frame we want to move
			deleteFrame(head, name);//deleting that frame
			break;
		}
		first = first->next;
	}
	insertToIndex(head, move, index - ONE);//sending to function that insert the frame at his new direction
	// i am sending it in -1 because it is easier for me to count from zero
}

/*
goal: to insert new frame at list in specific index
input: the list, the frame to enter, the index to enter the frame at
output: none
*/
void insertToIndex(FrameNode** head, FrameNode* newNode, int index)
{
	FrameNode* copy = NULL;
	int i = 0;
	if (index == i) {// if it is at the first spot
		newNode->next = (*head);
		(*head) = newNode;
	}
	else {
		copy = *head;// so we only change the original list but not delete when moving
		while (i != (index - ONE) && copy != NULL) {//while the location is less than this location minus one move to next and add 1 to location
		// i did it with index - 1 because i want to insert the newNode after the past location which is: index - 1
			copy = copy->next;
			i++;
		}
		newNode->next = copy->next;
		copy->next = newNode;
	}
}


/*
goal: to change duration for specific frame
input: pointer to the video list, name to change it duration, duration to change to
output: none
i am sending to function only after checking the name is in the list 
*/
void changeFrameDuration(FrameNode** list, char* name, int duration) {
	FrameNode* first = NULL;
	first = (*list);
	if ((strcmp(first->frame->name, name) == EQUAL)) {
		first->frame->duration = duration;
 	}
	else {
		while (first != NULL) {
			if (strcmp((first->frame->name), name) == EQUAL) {
				first->frame->duration = duration;
				return;
			}
			first = first->next;
		}
	}
}

/*
goal: to change name for specific frame
input: pointer to the video list, name to change it name, name to change to
output: none
i am sending to function only after checking the name is in the list 
*/
void changeFrameName(FrameNode** list, char* name, char* newName) {
	FrameNode* first = NULL;
	first = (*list);
	if ((strcmp(first->frame->name, name) == EQUAL)) {
		strcpy(first->frame->name, newName);
 	}
	else {
		while (first != NULL) {
			if (strcmp((first->frame->name), name) == EQUAL) {
				strcpy(first->frame->name, newName);
				return;
			}
			first = first->next;
		}
	}
}

/*
goal: to change path for specific frame
input: pointer to the video list, name to change it path, path to change to
output: none
i am sending to function only after checking the name is in the list 
*/
void changeFramePath(FrameNode** list, char* name, char* newPath) {
	FrameNode* first = NULL;
	first = (*list);
	if ((strcmp(first->frame->name, name) == EQUAL)) {
		strcpy(first->frame->path, newPath);
 	}
	else {
		while (first != NULL) {
			if (strcmp((first->frame->name), name) == EQUAL) {
				strcpy(first->frame->path, newPath);
				return;
			}
			first = first->next;
		}
	}
}


/*
goal: to remove the first frame
input: the head of the list
output: none
*/
void delteFirstFrame(FrameNode** head) {
	FrameNode* first = NULL;
	first = (*head);
	(*head) = (*head)->next;
	free(first);
	first = NULL;
}

/*
goal: to check if file is exist(it is bunary because we use here only binary files
input: the file to check
output: if the file exist\ openable (0 for no else for yes)
*/
int binaryFileExist(char* filename) {
	FILE* file = NULL;
	if (file = fopen(filename, "rb")) {
		fclose(file);
		return TRUE;
	}
	return FALSE;
}