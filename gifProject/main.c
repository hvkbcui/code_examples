/*********************************
* Class: MAGSHIMIM C2			 *
* openCV template      			 *
**********************************/
#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS

#define CV_IGNORE_DEBUG_BUILD_GUARD
#include <opencv2/imgcodecs/imgcodecs_c.h>
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#include <stdio.h>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui_c.h>
#include "unChange.h"
#include "linkedList.h"
#include "view.h"
#include "fileSave.h"

#define NOT_ZERO -1
#define EXIT 0
#define OK 1
#define ONE 1
#define ADD_FRAME 1
#define DELETE_FRAME 2
#define CHANGE_LOCATION 3
#define CHANGE_FRAME_DUR 4
#define CHANGE_ALL_DUR 5
#define PRINT_FRAME_LIST 6
#define PLAY_GIF 7
#define SAVE_FRAMES 8
#define REVERSE 9
#define CHANGE_FRAME_NAME 10
#define CHANGE_FRAME_PATH 11
#define NUM_REPITATION 12
#define DEFAULT_REPITATION 12


int main(void)
{
	FrameNode* head = NULL;
	char* newPath = NULL;
	char* path = NULL;
	char* name = NULL;
	char* newName = NULL;
	int choice = 0, index = 0, res = 0;
	int duration = 1000, gifRepeat = 0;
	newName = (char*)malloc(sizeof(char) * MAX_LEN + ONE);
	name = (char*)malloc(sizeof(char) * MAX_LEN + ONE);
	path = (char*)malloc(sizeof(char) * MAX_LEN + ONE);
	newPath = (char*)malloc(sizeof(char) * MAX_LEN + ONE);
	gifRepeat = DEFAULT_REPITATION;
	choice = printSaveMenu();
	if (choice == OK) {
		 choice = openExist(path);
		 if (!choice) {//if choice == 0 than the file can not be open
			 printf("Error!- cant open file, creating a new project");
		 }
		 else {// if choice != 0 than we can add the file data to head
			 head = retFrame(path);// ret frame extract the data out of the path and returns frame node with the details
			 printf("Project loaded successfully\n");
		 }
	}
	else {
		printf("Working on a new project.\n");
	}
	choice = NOT_ZERO;// so it keep the while
	while (choice != EXIT) {
		choice = menuChoice();
		switch (choice)
		{
		case EXIT:
			break;

		case ADD_FRAME:
			addFrame(&head);
			break;

		case DELETE_FRAME:
			res = getNameDelete(head, name);// checks if the name is in the list
			if (res) {//if yes, than delete the frame
				deleteFrame(&head, name);
			}
			break;

		case CHANGE_LOCATION:
			res = getNameIndex(head, name, &index);//0  if the indexes are not fine or the name is not fine but if yes than insert to location
			// i am sending pointer to index so i can put the value inside index without have to return it
			if (res) {
				insertToLocation(&head, name, index);
			}
			break;

		case CHANGE_FRAME_DUR:
			res = getFrameDuration(head, name, &duration);// if the duration is positive and the name exist
			// i am sending pointer to duration so i can put the value inside duration without have to return it
			if (res) {
				changeFrameDuration(&head, name, duration);
			}
			break;

		case CHANGE_ALL_DUR:
			res = getDurationForAll(head, &duration);// checks if the duration is positive
			if (res) {
				changeAllDuration(&head, duration);
			}
			break;

		case PRINT_FRAME_LIST:
			printList(head);
			break;

		case PLAY_GIF:
			play(head, gifRepeat);// sending number of repeatation for the gif(i added that variable)(bonus)
			break;

		case SAVE_FRAMES:
			printf("Where to save the project? enter a full path and file name\n");
			myFgets(path, MAX_LEN);
			saveProject(head, path);
			break;

		case REVERSE:
			endToStart(&head);
			printf("reversed!!\n");
			break;

		case CHANGE_FRAME_NAME:
			res = getFrameName(head, name, newName);// check if the name is in the list and the new name is not in the list
			if (res) {
				changeFrameName(&head, name, newName);
			}
			break;

		case CHANGE_FRAME_PATH:
			res = getFramePath(head, name, newPath);// check if the name is in the list and the new path exists
			if (res) {
				changeFramePath(&head, name, newPath);
			}
			break;

		case NUM_REPITATION:
			printf("Enter number of repeatition you want (default 5): ");
			scanf("%d", &gifRepeat);// gifRepeat is the variable we send in 7 when we play the gif so when we update here it update there too
			getchar();
			if (gifRepeat < ZERO) {
				printf("you can not change to minus! - gif repeat now is default\n");
				gifRepeat = DEFAULT_REPITATION;// restarting it to be 5 again
				break;
			}
			break;

		default:
			break;

		}
	}
	return ZERO;
}
#endif