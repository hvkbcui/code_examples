#ifndef UNCHANGEH
#define UNCHANGEH


#include <stdio.h>
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui_c.h>
#include "LinkedList.h"


/*
goal: to print menu and return choice 
input: none
output: choice
*/
int menuChoice();

/*
goal: to print menu 
input: none
output: none
*/
void printMenu();

/*
goal: to search for frame with his frame name in list of frames
input: linked list with frames, name to find
output: 0 if name not found else if found
*/
int searchName(FrameNode* list, char* name);
/*
goal: to return how many frames there is in the list
input: head - the first node of the list
output: how many frames
*/
int getLineLength(FrameNode* head);

/*
goal: to get the name in the delete action and return if the frame is in the list
input: the list to search name, the name pointer to enter the name and to check if is in the list
output: 0 if the name is not in the list else if in the list
*/
int getNameDelete(FrameNode* head, char* name);

/*
goal: to get the name and index  in the index action and return if the frame is in the list
input: the list to search name, the name pointer to enter the name and to check if is in the list
output: 0 if the name is not in the list else if in the list
*/
int getNameIndex(FrameNode* head, char* name, int* index);

/*
goal: to change path for specific frame
input: pointer to the video list, name to change it path, new path to change to
output: if the values are ok or not ( 0 if not else if yes)
*/
int getFramePath(FrameNode** head, char* name, char* newPath);

/*
goal: to change name for specific frame name
input: pointer to the video list, name to change it name, new name to change to
output: if the values are ok or not ( 0 if not else if yes)
*/
int getFrameName(FrameNode** head, char* name, char* newName);

/*
goal: to change duration for specific frame
input: pointer to the video list, name to change it duration, duration to change to
output: none
i am sending to function only after checking the name is in the list
*/
int getFrameDuration(FrameNode** head, char* name, int* duration);

/*
goal: to change duration for all frames
input: pointer to the video list, duration to change all frames to
output: none
*/
int getDurationForAll(FrameNode** head, int* duration);

/*
goal: to print the list frames
input: nodes of frame
output: none
*/
void printList(FrameNode* list);

/*
Function will perform the fgets command and also remove the newline
that might be at the end of the string - a known issue with fgets.
input: the buffer to read into, the number of chars to read
*/
void myFgets(char str[], int n);

#endif
#pragma once