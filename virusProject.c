
/*********************************
* Class: MAGSHIMIM C2			 *
* Week: 8             			 *
* Name: Yahel Navon              *
* Credits:                       *
**********************************/


#endif /*DIRENT_H*/

#include <stdio.h>
#include <string.h>
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>

int numFiles = 0;

#define NORMAL_SCAN 100
#define NORMAL_SCAN_DOT 100.0
#define QUICK_SCAN 20
#define QUICK_SCAN_DOT 20.0
#define INFECTED 0
#define CLEAN 1
#define FIRST 2
#define LAST 3
#define MAX_LENGTH 50

int checkPaths(char* scanFolder, char* virSignt);
int binaryFileExist(char* filename);
int printMenu(char* scanFolder, char* virSignt);
void sendToScan(char* scanFolder, char* virSignt, int scan);
int scan(char* scanFile, char* virSignt, int scanPercent);
void replaceBackSlash(char* path);
int filesNum(char* scanFolder);

int main(int argc, char** argv)
{
    int scanType = 0;//if scan == 0 normal scan else quick scan
    replaceBackSlash(argv[1]);
    replaceBackSlash(argv[2]);
    if (checkPaths(argv[1], argv[2]) == 0) {
        printf("Could not open the virus or one of the files...");
        return;
    }
    else {
        scanType = printMenu(argv[1], argv[2]);
        sendToScan(argv[1], argv[2], scanType);
    }
	getchar();
	return 0;
}
/*
goal: to replace backslash -\ with slash - / because it is pointer i can change it from the function
input: path to replace \
output: none
*/
void replaceBackSlash(char* path) {
    int i = 0;
    for (i = 0; i < strlen(path); i++) {
        if (path[i] == '\\') {
            path[i] = '/';
        }
    }
}
/*
goal: to return how many files there is in folder
input: folder
output: num of files in it
*/
int filesNum(char* scanFolder) {
    DIR* d = 0;
    struct dirent* dir = 0;
    int numFiles = 0;
    d = opendir(scanFolder);
    if (d == NULL)
    {
        printf("Error opening directory!");
        return 0;
    }
    dir = readdir(d);
    while ((dir = readdir(d)) != NULL)
    {
        if (strcmp(dir->d_name, ".") && strcmp(dir->d_name, "..") && dir->d_type != DT_DIR) {
            numFiles++;
        }
    }
    return numFiles;
}
/*
goal: to check in directory if all the files are ok and ready to open by sending each file to function that check it 
	and to check the virus signature if it is not NULL
input: the folder to check all files are openable and the virus signature to check the same
output: if all the files are ok (0 for no 1 for yes)
*/
int checkPaths(char* scanFolder, char* virSignt) {
    DIR* d = 0;
    char* path = NULL;
    char** names = NULL;
    struct dirent* dir = 0;

    d = opendir(scanFolder);
    if (d == NULL)
    {
        printf("Error opening directory!");
        return 0;
    }
    while ((dir = readdir(d)) != NULL)
    {
        if (strcmp(dir->d_name, ".") && strcmp(dir->d_name, "..") && dir->d_type != DT_DIR){
            path = (char*)malloc(sizeof(char) * (strlen(scanFolder) + strlen(dir->d_name) + 2));
            strcpy(path, scanFolder);
            strcat(path, "/");
            strcat(path, dir->d_name);
            if (binaryFileExist(path) == 0) {
                closedir(d);
                free(path);
                return 0;
            }
            free(path);
            numFiles++;
        }
    }
    if (binaryFileExist(virSignt) == 0) {
        return 0;
    }
    closedir(d);
    return 1;
}

/*
goal: to check if file is exist(it is bunary because we use here only binary files
input: the file to check
output: if the file exist\ openable (0 for no 1 for yes)
*/
int binaryFileExist(char* filename) {
	FILE* file = NULL;
	if (file = fopen(filename, "rb")) {
		fclose(file);
		return 1;
	}
	return 0;
}

/*
goal: to print menu with the folder and the choice if to do the quick or normal scan and to return choice
input: the folder to scan and the virus signature
output: the users choice (0 for normal scan else to quick)
*/
int printMenu(char* scanFolder, char* virSignt) {
    int choice = 0;
    printf("Welcome to my Virus Scan!\n\n");
    printf("Folder to scan: %s\n", scanFolder);
    printf("Virus signature: %s\n\n", virSignt);
    printf("Press 0 for a normal scan or any other key for a quick scan: ");
    scanf("%d", &choice);
    return choice;
}
/*
goal: to get each file out of the folder and to send him to function
	that checks in normal scan or in quick scan if the file is ok or not and to print results i could put this in check paths but it is less readable
    and the function creates file with all the details of the scan
input: the folder path and the virus signatur path and what kind of scan( 0 for normal 1 to quick) or else than 1
ouput: none
*/
void sendToScan(char* scanFolder, char* virSignt, int scanType) {
    DIR* d = 0;
    FILE* log = NULL;
    struct dirent* dir = 0;
    char* path = NULL;
    int quickRes = 0;
    char antiLog[18] = "AntiVirusLog.txt";
    d = opendir(scanFolder);
    if (d == NULL)
    {
        printf("Error opening directory");
        return;
    }
    log = fopen(antiLog, "w");
    if (log == NULL) {
        printf("Error opening file.\n");
        return;
    }
    fputs("Anti Virus began! Welcome\n\n", log);
    fputs("Folder to scan:\n", log);
    fputs(scanFolder, log);
    fputs("\nVirus signature:\n", log);
    fputs(virSignt, log);
    fputs("\n\nScanning option:\n", log);
    if (scanType == 0) {
        fputs("Normal Scan\n", log);
    }
    else {
        fputs("Quick Scan\n\n", log);
    }
    fputs("Results:\n", log);
    printf("Scanning began...\n");
    printf("This process may take several minutes...\n\n");
    printf("Scanning:\n");
    
    while ((dir = readdir(d)) != NULL)
    {
        if (strcmp(dir->d_name, ".") && strcmp(dir->d_name, "..") &&
            dir->d_type != DT_DIR)
        {
            path = (char*)malloc(sizeof(char) * (strlen(scanFolder) + strlen(dir->d_name) + 2));
            strcpy(path, scanFolder);
            strcat(path, "/");
            strcat(path, dir->d_name);
            
            printf("\n%s - ", path);
            fputs(path, log);
            if (scanType == 0) {
                if (scan(path, virSignt, NORMAL_SCAN)) {
                    printf("Infected!");
                    fputs(" Infected!\n", log);
                }
                else {
                    printf("Clean");
                    fputs(" Clean\n", log);
                }
            }
            else {
               quickRes = scan(path, virSignt, QUICK_SCAN);
               switch (quickRes)
               {
               case 0: 
                   printf("Clean");
                   fputs(" Clean\n", log);
                   break;
               case 1: 
                   printf("Infected!");
                   fputs(" Infected!\n", log);
                   break;
               case 2: 
                   printf("Infected! <first 20%>");
                   fputs(" Infected! (first 20%)\n", log);
                   break;
               case 3: 
                   printf("Infected! <last 20%>");
                   fputs(" Infected! (last 20%)\n", log);
                   break;
               }
            }    
            free(path);
        }
    }
    printf("\nScan Completed.\n");
    printf("See log path for results : %s/AntiVirusLog.txt\n", scanFolder);
    fclose(log);
    closedir(d);
}
/*
goal: to get file and to check if the file is with the virus signature or not
input: the folder path and the virus signatur path and how much presentege to check (if the scan is quick so 20 if normal 100)
ouput: 0 if the file is infected 1 if not in normal scan 2 of infected by the first 20 percent 3 if infected by the last 20 percent
*/
int scan(char* scanFile, char* virSignt, int scanPercent) {
    FILE* fFile = NULL;
    FILE* sFile = NULL;
    char ch = ' ';
    int i = 0, j = 0;
    size_t result = NULL;
    long fSize = 0, count = 0, lSize = 0;
    float percent = 0;// to calculate how many words
    char* virus = { ' ' };
    char* buffer = { ' ' };
    fFile = fopen(virSignt, "rb");
    sFile = fopen(scanFile, "rb");
    if (fFile == NULL) {
        printf("file error!");
        return;
    }
    fseek(fFile, 0, SEEK_END);
    fSize = ftell(fFile);
    rewind(fFile);
    virus = (char*)malloc(sizeof(char) * fSize);
    if (virus == NULL) {
        printf("memory error!");
        free(virus);
        fclose(fFile);
        fclose(sFile);
        return;
    }
    result = fread(virus, 1, fSize, fFile);
    if (result == NULL) {
        printf("read error!");
        free(virus);
        fclose(fFile);
        fclose(sFile);
        return;
    }
    if (sFile == NULL) {
        printf("scan file error!");
        free(virus);
        fclose(fFile);
        fclose(sFile);
        return;
    }
    fseek(sFile, 0, SEEK_END);
    lSize = ftell(sFile);
    rewind(sFile);
    buffer = (char*)malloc(sizeof(char) * lSize);
    if (buffer == NULL) {
        printf("memory error!");
        free(buffer);
        free(virus);
        fclose(fFile);
        fclose(sFile);
        return;
    }
    result = fread(buffer, 1, lSize, sFile);
    if (result == NULL) {
        printf("read error!");
        free(buffer);
        free(virus);
        fclose(fFile);
        fclose(sFile);
        return;
    }
    if (scanPercent == 20) {
        percent = 20.0 / 100.0;
        percent = percent * lSize;// that is how you calculate percent
        // i know i could use some loop here but that is just will complicate everything and make me do more variabels and i think this solution is better
        for (i = 0; i < percent; i++) {
            if (buffer[i] == virus[0]) {
                count = 0;
                for (j = 0; j < fSize; j++) {
                    if (j >= lSize || j >= percent) {
                        break;
                    }
                    if (buffer[i + j] == virus[j]) {
                        count++;
                    }
                }
                if (count == fSize) {
                    free(buffer);
                    free(virus);
                    fclose(fFile);
                    fclose(sFile);
                    return FIRST;
                }
            }
        }
        for (i = (lSize - percent); i < lSize; i++) {
            if (buffer[i] == virus[0]) {
                count = 0;
                for (j = 0; j < fSize; j++) {
                    if (j >= lSize) {
                    break;
                }
                    if (buffer[i + j] == virus[j]) {
                        count++;
                    }
                }
                if (count == fSize) {
                    free(buffer);
                    free(virus);
                    fclose(fFile);
                    fclose(sFile);
                    return LAST;
                }
            }
        }
        for (i = percent; i < (lSize - percent); i++) {
            if (buffer[i] == virus[0]) {
                count = 0;
                for (j = 0; j < fSize; j++) {
                    if (j >= fSize || j >= (lSize - percent)) {
                        break;
                    }
                    if (buffer[i + j] == virus[j]) {
                        count++;
                    }
                }
                if (count == fSize) {
                    free(buffer);
                    free(virus);
                    fclose(fFile);
                    fclose(sFile);
                    return CLEAN;
                }
            }
        }// doing everything after
        for (i = 0; i < lSize; i++) {
            if (buffer[i] == virus[0]) {
                count = 0;
                for (j = 0; j < fSize; j++) {
                    if (buffer[i + j] == virus[j]) {
                        count++;
                    }
                }
                if (count == fSize) {
                    free(buffer);
                    free(virus);
                    fclose(fFile);
                    fclose(sFile);
                    return CLEAN;
                }
            }
        }
    }
    else {
        for (i = 0; i < lSize; i++) {
            if (buffer[i] == virus[0]) {
                count = 0;
                for (j = 0; j < fSize; j++) {
                    if (buffer[i + j] == virus[j]) {
                        count++;
                    }
                }
                if (count == fSize) {
                    free(buffer);
                    free(virus);
                    fclose(fFile);
                    fclose(sFile);
                    return CLEAN;
                }
            }
        }
    }
    free(buffer);
    free(virus);
    fclose(fFile);
    fclose(sFile);
    return INFECTED;
}
